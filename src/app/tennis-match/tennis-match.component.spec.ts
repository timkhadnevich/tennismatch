import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TennisMatchComponent } from './tennis-match.component';

describe('TennisMatchComponent', () => {
  let component: TennisMatchComponent;
  let fixture: ComponentFixture<TennisMatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TennisMatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TennisMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
