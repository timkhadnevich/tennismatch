import { Component, OnInit } from '@angular/core';

export interface Set {
  playerOneScore: number;
  playerTwoScore: number;
  matchState: StateOfMatchEnum
  playerOneGameScore: number;
  playerTwoGameScore: number;
}

export enum StateOfMatchEnum {
  NO_RESULT,
  PLAYER_ONE_WON,
  PLAYER_TWO_WON,
  PLAYER_ONE_WON_40,
  PLAYER_TWO_WON_40,
  PLAYER_ONE_ADVANTAGE,
  PLAYER_TWO_ADVANTAGE,
  DEUCE
}

export interface Game {
  sets: Set[],
  currentStateOfMatch: StateOfMatchEnum,
}

@Component({
  selector: 'tennis-match',
  templateUrl: './tennis-match.component.html',
  styleUrls: ['./tennis-match.component.scss']
})
export class TennisMatchComponent implements OnInit {

  private AVAILABLE_SCORES = [0, 15, 30, 40];
  private DEUCE_SCORE = 40;

  private DEUCE_RESULT_STRING = "Deuce";
  private ONE_WON_RESULT_STRING = "Player 1 wins!";
  private TWO_WON_RESULT_STRING = "Player 2 wins";

  isEndGame = false;

  currentGame: Game;

  constructor() { }

  ngOnInit() {
    this.currentGame = {} as Game;
    let sets: Set[] = [];
    this.currentGame.sets = sets;
    this.currentGame.currentStateOfMatch = StateOfMatchEnum.NO_RESULT;
    this.isEndGame = false;
  }

  resetGame() {
    this.ngOnInit();
  }


  playGame() {
    let newSet = {} as Set;
    newSet.playerOneScore = this.getRandomScore();
    newSet.playerTwoScore = this.getRandomScore();


    //Update state of set inside the game
    newSet.matchState = this.getStateOfMatch(newSet);
    this.currentGame.currentStateOfMatch = newSet.matchState;

    //Update set score inside the set
    this.updateGameScore(newSet);

    this.currentGame.sets.push(newSet);

    this.isEndGame = this.getGameResultForSet(newSet) === this.TWO_WON_RESULT_STRING ||  this.getGameResultForSet(newSet) === this.ONE_WON_RESULT_STRING;

  }

  getGameResultForSet(set: Set) {
    if(set.playerTwoGameScore == 6) {
      return this.TWO_WON_RESULT_STRING;
    }

    if(set.playerOneGameScore == 6) {
      return this.ONE_WON_RESULT_STRING;
    }

    if(set.matchState === StateOfMatchEnum.DEUCE) {
      return this.DEUCE_RESULT_STRING;
    }

    if(set.matchState === StateOfMatchEnum.PLAYER_ONE_WON) {
      return  this.ONE_WON_RESULT_STRING;
    }

    if(set.matchState === StateOfMatchEnum.PLAYER_TWO_WON) {
      return  this.TWO_WON_RESULT_STRING;
    }
    return set.playerOneScore + " - " + set.playerTwoScore;
  }

  private updateGameScore(set: Set) {

    let playerOneGameScore: number = 0;
    let playerTwoGameScore: number = 0;

    if(this.currentGame.sets.length > 0) {
      playerOneGameScore = this.currentGame.sets[this.currentGame.sets.length - 1].playerOneGameScore;
      playerTwoGameScore = this.currentGame.sets[this.currentGame.sets.length - 1].playerTwoGameScore;
    }

    let setGameState: StateOfMatchEnum = set.matchState;

    if(setGameState === StateOfMatchEnum.NO_RESULT) {
      if(set.playerOneScore > set.playerTwoScore ) {
        playerOneGameScore++;
      }

      if(set.playerTwoScore > set.playerOneScore) {
        playerTwoGameScore++;
      }
    }

    if(setGameState === StateOfMatchEnum.PLAYER_ONE_WON_40
      || setGameState === StateOfMatchEnum.PLAYER_ONE_ADVANTAGE
      || setGameState === StateOfMatchEnum.PLAYER_ONE_WON) {
      playerOneGameScore++;
    }

    if(setGameState === StateOfMatchEnum.PLAYER_TWO_WON_40
      || setGameState === StateOfMatchEnum.PLAYER_TWO_ADVANTAGE
      || setGameState === StateOfMatchEnum.PLAYER_TWO_WON) {
      playerTwoGameScore++;
    }

    set.playerOneGameScore = playerOneGameScore;
    set.playerTwoGameScore = playerTwoGameScore;
  }

  private getStateOfMatch(set: Set) {

    let currentState: StateOfMatchEnum = this.currentGame.currentStateOfMatch;

    let samePoints: boolean = set.playerOneScore === set.playerTwoScore;
    let winnerPlayerOne: boolean = set.playerOneScore > set.playerTwoScore;

    if(set.playerOneScore === this.DEUCE_SCORE &&
      set.playerTwoScore === this.DEUCE_SCORE) {
      return StateOfMatchEnum.DEUCE;
    }

    if(samePoints) {
      return StateOfMatchEnum.NO_RESULT;
    }

    if(currentState === StateOfMatchEnum.NO_RESULT) {

      if(set.playerOneScore === this.DEUCE_SCORE) {
        return StateOfMatchEnum.PLAYER_ONE_WON_40;
      }

      if(set.playerTwoScore === this.DEUCE_SCORE) {
        return StateOfMatchEnum.PLAYER_TWO_WON_40;
      }
    }

    if(currentState === StateOfMatchEnum.DEUCE) {

      if(set.playerOneScore === this.DEUCE_SCORE &&
        set.playerTwoScore === this.DEUCE_SCORE) {
        return StateOfMatchEnum.DEUCE;
      }

      if(winnerPlayerOne) {
        return StateOfMatchEnum.PLAYER_ONE_ADVANTAGE;
      } else {
        return StateOfMatchEnum.PLAYER_TWO_ADVANTAGE;
      }

    }

    let advantageAfterDeuce = currentState === StateOfMatchEnum.PLAYER_ONE_ADVANTAGE || currentState === StateOfMatchEnum.PLAYER_TWO_ADVANTAGE;
    let advantageAfter40 = currentState === StateOfMatchEnum.PLAYER_ONE_WON_40 || currentState === StateOfMatchEnum.PLAYER_TWO_WON_40;

    if(advantageAfterDeuce) {
      if(winnerPlayerOne &&  currentState === StateOfMatchEnum.PLAYER_ONE_ADVANTAGE) {
        return StateOfMatchEnum.PLAYER_ONE_WON;
      }

      if(!winnerPlayerOne &&  currentState === StateOfMatchEnum.PLAYER_TWO_ADVANTAGE) {
        return StateOfMatchEnum.PLAYER_TWO_WON;
      }

      if(winnerPlayerOne &&  currentState === StateOfMatchEnum.PLAYER_TWO_ADVANTAGE) {
        return StateOfMatchEnum.DEUCE;
      }

      if(!winnerPlayerOne &&  currentState === StateOfMatchEnum.PLAYER_ONE_ADVANTAGE) {
        return StateOfMatchEnum.DEUCE;
      }

      return StateOfMatchEnum.NO_RESULT;
    }

    if(advantageAfter40) {
        if (winnerPlayerOne && currentState === StateOfMatchEnum.PLAYER_ONE_WON_40) {
          return StateOfMatchEnum.PLAYER_ONE_WON;
        }

        if (!winnerPlayerOne && currentState === StateOfMatchEnum.PLAYER_TWO_WON_40) {
          return StateOfMatchEnum.PLAYER_TWO_WON;
        }

        if(set.playerOneScore === this.DEUCE_SCORE) {
          return StateOfMatchEnum.PLAYER_ONE_WON_40;
        }

        if(set.playerTwoScore === this.DEUCE_SCORE) {
          return StateOfMatchEnum.PLAYER_TWO_WON_40;
        }

        return StateOfMatchEnum.NO_RESULT;
    }

    return currentState;

  }

  private getRandomScore(): number {
    return this.AVAILABLE_SCORES[Math.floor(Math.random() * this.AVAILABLE_SCORES.length)]
  }

}
